const fs = require('fs');
const sass = require('node-sass');

var result = sass.renderSync({
    file: './scss/alanleouk-ui.scss',
    outputStyle: 'nested',
    sourceMap: false
});

fs.writeFileSync(`./css/alanleouk-ui.css`, result.css);
