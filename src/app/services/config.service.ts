import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

export class ConfigModel {
  public brandUseLogo = false;
}

@Injectable()
export class ConfigService {

  public loaded$ = new Subject();
  public refresh$ = new Subject();
  public current: ConfigModel;

  constructor(
    private http: HttpClient
  ) {

  }

  public refresh() {
    this.refresh$.next();
  }

  load(): void {
    this.http.get<ConfigModel>('/config.json').subscribe(result => {
      this.current = result;
      this.loaded$.next();
    });
  }
}
