import { Injectable, ComponentFactoryResolver, Type, ApplicationRef, Injector, EmbeddedViewRef, ComponentRef } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';

export interface IDialogComponent<T, TResult> {
  dialog: Dialog<T, TResult>;
}

export class Dialog<T, TResult> {

  private afterClosedSubject$ = new Subject<TResult>();

  public options: DialogOptions;
  public result: TResult;
  public componentRef: ComponentRef<T>;
  public componentInstance: T;

  constructor(private modalService: ModalService) {

  }

  show$(): Observable<TResult> {
    this.show();
    return this.afterClosedSubject$;
  }

  show() {
    this.modalService.show(this);
  }

  close() {
    this.modalService.close(this);
    this.afterClosedSubject$.next(this.result);
  }
}

export class DialogOptions {
  public width: string;
  public height: string;
}

export class DialogResult {
  public success: boolean;
}

@Injectable()
export class ModalService {

  private modalContainerId = 'modalContainer';
  private modalContentId = 'modalContent';
  private overlayElementId = 'modalOverlay';

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector) {

  }

  confirm$(message: string): Observable<DialogResult> {
    return of({
      success: confirm(message)
    });
  }

  create<T extends IDialogComponent<T, TResult>, TResult>(component: Type<T>, options: DialogOptions): Dialog<T, TResult> {
    const factory = this.componentFactoryResolver.resolveComponentFactory(component);
    const componentRef = factory.create(this.injector);
    this.appRef.attachView(componentRef.hostView);

    const dialog = new Dialog<T, TResult>(this);
    dialog.options = options;
    dialog.componentRef = componentRef;
    dialog.componentInstance = dialog.componentRef.instance;
    componentRef.instance.dialog = dialog;
    return dialog;
  }

  show<T, TResult>(dialog: Dialog<T, TResult>) {
    const domElem = (dialog.componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    // document.body.appendChild(domElem);
    const modalContent = document.getElementById(this.modalContentId);
    if (dialog.options.width) {
      modalContent.style.width = dialog.options.width;
    }
    if (dialog.options.height) {
      modalContent.style.height = dialog.options.height;
    }
    modalContent.appendChild(domElem);

    document.getElementById(this.modalContainerId).className = 'modal-container';
    document.getElementById(this.overlayElementId).className = 'modal-overlay';
  }

  close<T, TResult>(dialog: Dialog<T, TResult>) {

    document.getElementById(this.modalContainerId).className = 'modal-container d-none';
    document.getElementById(this.overlayElementId).className = 'modal-overlay d-none';

    this.appRef.detachView(dialog.componentRef.hostView);
    dialog.componentRef.destroy();
  }
}
