import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home';
import { SpinnerComponent } from './components/spinner/spinner';
import { CardsComponent } from './components/cards/cards';
import { ButtonsComponent } from './components/buttons/buttons';
import { FormsComponent } from './components/forms/forms';
import { ModalsComponent } from './components/modals/modals';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'spinner', component: SpinnerComponent },
  { path: 'buttons', component: ButtonsComponent },
  { path: 'cards', component: CardsComponent },
  { path: 'forms', component: FormsComponent },
  { path: 'modals', component: ModalsComponent },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
