import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { HomeComponent } from './components/home/home';
import { SpinnerComponent } from './components/spinner/spinner';
import { CardsComponent } from './components/cards/cards';
import { ButtonsComponent } from './components/buttons/buttons';
import { FormsComponent } from './components/forms/forms';
import { ConfigService } from './services/config.service';
import { HttpClientModule } from '@angular/common/http';
import { ModalService } from './services/modal.service';
import { ModalsComponent } from './components/modals/modals';
import { Modal1Component } from './components/modals/modal-1';

@NgModule({
  declarations: [
    AppComponent,
    ButtonsComponent,
    CardsComponent,
    FormsComponent,
    HomeComponent,
    ModalsComponent,
    Modal1Component,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    ConfigService,
    ModalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
