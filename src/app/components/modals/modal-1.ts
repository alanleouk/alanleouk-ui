import { Component, Optional } from '@angular/core';
import { DialogResult, Dialog } from 'src/app/services/modal.service';

@Component({
  selector: 'app-modal-1',
  templateUrl: './modal-1.html',
  styleUrls: []
})
export class Modal1Component {
  constructor(@Optional() public dialog: Dialog<Modal1Component, DialogResult>) {

  }
}
