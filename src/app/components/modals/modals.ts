import { Component } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';
import { Modal1Component } from './modal-1';

@Component({
  selector: 'app-modals',
  templateUrl: './modals.html',
  styleUrls: []
})
export class ModalsComponent {

  constructor(public modalService: ModalService) {
    this.modal1();
  }

  modal1() {
    const modal = this.modalService.create(Modal1Component, { width: '800px', height: 'auto' });
    modal.show();
  }
}
