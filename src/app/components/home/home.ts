import { Component } from '@angular/core';
import { ConfigService } from '../../services/config.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.html',
  styleUrls: []
})
export class HomeComponent {

  constructor(public config: ConfigService) {
  }
}
